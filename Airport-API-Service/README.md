# Airport API Service

Welcome to the Airport API project! This API is designed to manage airplanes, airports, routes, crews and flights in an airport service and to make orders for flights.

## Table of Contents

- [Features](#features)
- [Technologies Used](#technologies-used)
- [Getting Started](#getting-started)
  - [Prerequisites](#prerequisites)
  - [Installation](#installation)
- [API Endpoints](#api-endpoints)
- [Credentials](#credentials)
- [DB Structure](#db-structure)

## Features

- Manage airports and their details.
- Manage routes between different airport
- Schedule and manage flights using different routes.
- Allow users to make orders for flights.
- User authentication and authorization.
- Throttle API requests to prevent abuse.
- Add images for airplanes.

## Technologies Used
* Django
* Django REST framework
* Docker
* JWT Authentication
* Swagger/OpenAPI Documentation


## Getting Started

### Prerequisites
* Python (version 3.6 or higher) and pip installed on your system
* Git (optional, for cloning the repository)
* Start docker engine for running in docker


### Installation
To set up the Airport API project follow these steps:
```shell
 git clone https://gitlab.com/persie.iovchu/airport-api-service
 cd airport-api-service
 python -m venv venv
 venv\Scripts\activate

-----------------------------------------------

 pip install requirements.txt  # for sqlite3.db
 python loaddata data.json
 python manage.py migrate
 python manage.py runserver
 
-----------------------------------------------

 docker-compose up --build  # for postgresql.db
```
## API Endpoints
```
"airport":
    "airplane_types": "http://127.0.0.1:8000/api/airport/airplane_types/",
    "airplanes": "http://127.0.0.1:8000/api/airport/airplanes/",
    "airports": "http://127.0.0.1:8000/api/airport/airports/",
    "crews": "http://127.0.0.1:8000/api/airport/crews/",
    "routes": "http://127.0.0.1:8000/api/airport/routes/"
    "flights": "http://127.0.0.1:8000/api/airport/flights/"
    "orders": "http://127.0.0.1:8000/api/airport/orders/"

"user":
    "registration": "http://127.0.0.1:8000/api/user/register/"
	"token": "http://127.0.0.1:8000/api/user/token/"

"documentatoin": 
	"swagger": "http://127.0.0.1:8000/api/swagger/" 
	"redoc": "http://127.0.0.1:8000/api/redoc/"
```

## Credentials
To check the functionality of my Airport API Service you can use the following credentials
```
email: aviator@admin.com
password: 1w2q3r4e5y6t
```

## DB Structure
![db structure](db_structure.jpg)