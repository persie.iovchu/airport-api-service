from datetime import datetime

from django.contrib.auth import get_user_model
from django.db.models import F, Count
from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework import status

from rest_framework.test import APIClient

from airport.models import (
    AirplaneType,
    Airplane,
    Airport,
    Route,
    Crew,
    Flight,
    Ticket,
)
from airport.serializers import FlightListSerializer, FlightRetrieveSerializer

FLIGHT_URL = reverse("airport:flight-list")


def sample_airplane_type(**params):
    dafaults = {
        "name": "Sample airplane type"
    }
    dafaults.update(params)

    return AirplaneType.objects.create(**dafaults)


def sample_airplane(**params):
    airplane_type = sample_airplane_type(name="Sample airplane type")

    defaults = {
        "name": "Sample airplane",
        "rows": 10,
        "seats_in_row": 100,
        "airplane_type": airplane_type
    }
    defaults.update(params)

    return Airplane.objects.create(**defaults)


def sample_airport(**params):
    defaults = {
        "name": "Sample airport",
        "closest_big_city": "Sample city"
    }
    defaults.update(params)

    return Airport.objects.create(**defaults)


def sample_route(**params):
    airport1 = sample_airport(name=params.get("source_name", "Airport 1"))
    airport2 = sample_airport(name=params.get("destination_name", "Airport 2"))
    defaults = {
        "source": airport1,
        "destination": airport2,
        "distance": 500
    }
    defaults.update(params)

    return Route.objects.create(**defaults)


def sample_crew(**params):
    defaults = {
        "first_name": "Sample first name",
        "last_name": "Sample last name"
    }
    defaults.update(params)

    return Crew.objects.create(**defaults)


def sample_flight(**params):
    route = sample_route()
    airplane = sample_airplane()
    defaults = {
        "route": route,
        "airplane": airplane,
        "departure_time": "2023-10-11 14:00:00",
        "arrival_time": "2022-10-11 14:50:00"
    }
    defaults.update(params)

    return Flight.objects.create(**defaults)


def detail_url(airplane_id):
    return reverse("airport:flight-detail", args=[airplane_id])


class UnauthenticatedFlightApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_auth_required(self):
        res = self.client.get(FLIGHT_URL)
        self.assertEqual(res.status_code, status.HTTP_401_UNAUTHORIZED)


class AuthenticatedFlightApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            "test@test.com",
            "password",
        )
        self.client.force_authenticate(self.user)

        airport_source1 = sample_airport(name="Madrid")
        airport_source2 = sample_airport(name="Barcelona")
        airport_destination1 = sample_airport(name="London")
        airport_destination2 = sample_airport(name="Manchester")

        self.route1 = Route.objects.create(source=airport_source1, destination=airport_destination1, distance=900)
        self.route2 = Route.objects.create(source=airport_source2, destination=airport_destination2, distance=800)

        airplanetype1 = AirplaneType.objects.create(name="AirplaneType 1")
        airplanetype2 = AirplaneType.objects.create(name="AirplaneType 2")

        self.airplane1 = Airplane.objects.create(name="Airplane 1", rows=10, seats_in_row=10, airplane_type=airplanetype1)
        self.airplane2 = sample_airplane(name="Airplane 2", rows=10, seats_in_row=10, airplane_type=airplanetype2)

        self.flight1 = Flight.objects.create(
            route=self.route1,
            airplane=self.airplane1,
            departure_time=datetime(2022, 12, 12, tzinfo=timezone.utc),
            arrival_time=datetime(2022, 12, 12, tzinfo=timezone.utc)
        )
        self.flight2 = Flight.objects.create(
            route=self.route2,
            airplane=self.airplane2,
            departure_time=datetime(2022, 10, 12, tzinfo=timezone.utc),
            arrival_time=datetime(2022, 10, 13, tzinfo=timezone.utc)
        )
        self.serializer1 = FlightListSerializer(self.flight1)
        self.serializer2 = FlightListSerializer(self.flight2)

    def test_flight_annotate(self):
        """Test of custom annotate fields for flight without orders and without bought tickets"""

        flight1 = self.flight1
        res = self.client.get(FLIGHT_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertIn("tickets_available", res.data[0])

        bought_tickets = Ticket.objects.filter(flight=flight1.id).count()
        tickets_available = flight1.airplane.capacity - bought_tickets
        crew_size = Crew.objects.filter(flight=flight1.id).count()

        self.assertEqual(res.data[0]["tickets_available"], tickets_available)

    def test_list_flights(self):
        res = self.client.get(FLIGHT_URL)

        flights = Flight.objects.all().annotate(
            tickets_available=F("airplane__rows") * F("airplane__seats_in_row") - Count("tickets"),
            crew_size=Count("crew__id")
        )
        serializer = FlightListSerializer(flights, many=True)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_filter_flight_by_source(self):
        res = self.client.get(FLIGHT_URL, {"source": "barc"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer2.data, res.data)
        self.assertNotIn(self.serializer1.data, res.data)

    def test_filter_flight_by_destination(self):

        res = self.client.get(FLIGHT_URL, {"destination": "london"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer1.data, res.data)
        self.assertNotIn(self.serializer2.data, res.data)

    def test_filter_by_departure_time(self):
        res = self.client.get(FLIGHT_URL, {"departure_time": "2022-12-12"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer1.data, res.data)
        self.assertNotIn(self.serializer2.data, res.data)

        res = self.client.get(FLIGHT_URL, {"departure_time": "2022-10-12"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer2.data, res.data)

    def test_filter_by_arrival_time(self):
        res = self.client.get(FLIGHT_URL, {"arrival_time": "2022-12-12"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer1.data, res.data)
        self.assertNotIn(self.serializer2.data, res.data)

        res = self.client.get(FLIGHT_URL, {"arrival": "2022-10-13"})

        field_names_to_remove = ["tickets_available"]

        res.data = [
            {key: value for key, value in item.items() if key not in field_names_to_remove} for item in res.data
        ]

        self.assertIn(self.serializer2.data, res.data)

    def test_retrieve_flight_detail(self):
        flight = self.flight1

        url = detail_url(flight.id)
        res = self.client.get(url)

        serializer = FlightRetrieveSerializer(flight)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data, serializer.data)

    def test_create_flight_forbidden(self):
        payload = {
            "departure_time": "2023-04-11",
            "arrival_time": "2023-04-12",
            "route": self.route1,
            "airplane": self.airplane1
        }

        res = self.client.post(FLIGHT_URL, payload)

        self.assertEqual(res.status_code, status.HTTP_403_FORBIDDEN)


class AdminFlightApiTests(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            "admin@admin.com", "testpass", is_staff=True
        )
        self.client.force_authenticate(self.user)

        airport_source1 = sample_airport(name="Madrid")
        airport_destination1 = sample_airport(name="London")

        self.route1 = Route.objects.create(source=airport_source1, destination=airport_destination1, distance=900)

        airplanetype1 = AirplaneType.objects.create(name="AirplaneType 1")

        self.airplane1 = Airplane.objects.create(name="Airplane 1", rows=10, seats_in_row=10, airplane_type=airplanetype1)

    def test_create_flight(self):
        payload = {
            "departure_time": "2023-04-11",
            "arrival_time": "2023-04-12",
            "route": self.route1.id,
            "airplane": self.airplane1.id
        }

        res = self.client.post(FLIGHT_URL, payload)

    def test_put_flight_not_allowed(self):
        payload = {
            "departure_time": "2023-04-11",
            "arrival_time": "2023-04-12",
            "route": self.route1.id,
            "airplane": self.airplane1.id
        }

        flight = sample_flight()
        url = detail_url(flight.id)

        res = self.client.put(url, payload)

        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete_flight_not_allowed(self):
        flight = sample_flight()
        url = detail_url(flight.id)

        res = self.client.delete(url)

        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
