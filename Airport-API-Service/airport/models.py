import os
import uuid

from django.conf import settings
from django.db import models
from django.utils.text import slugify
from rest_framework.exceptions import ValidationError


class AirplaneType(models.Model):
    name = models.CharField(max_length=63, unique=True)

    class Meta:
        app_label = "airport"

    def __str__(self):
        return self.name


def airplane_image_file_path(instance, filename):
    _, extension = os.path.splitext(filename)
    filename = f"{slugify(instance.name)}-{uuid.uuid4()}{extension}"

    return os.path.join("uploads/airplanes/", filename)


class Airplane(models.Model):
    name = models.CharField(max_length=63)
    rows = models.IntegerField()
    seats_in_row = models.IntegerField()
    airplane_type = models.ForeignKey(
        to=AirplaneType,
        related_name="airplanes",
        on_delete=models.CASCADE
    )
    image = models.ImageField(null=True, upload_to=airplane_image_file_path)

    class Meta:
        app_label = "airport"

    @property
    def capacity(self):
        return self.rows * self.seats_in_row

    def __str__(self):
        return self.name


class Airport(models.Model):
    name = models.CharField(max_length=63, unique=True)
    closest_big_city = models.CharField(max_length=63)

    class Meta:
        app_label = "airport"

    def __str__(self):
        return self.name


class Route(models.Model):
    source = models.ForeignKey(
        to=Airport,
        related_name="source_routes",
        on_delete=models.CASCADE
    )
    destination = models.ForeignKey(
        to=Airport,
        related_name="destination_routes",
        on_delete=models.CASCADE
    )
    distance = models.IntegerField()

    class Meta:
        app_label = "airport"

    @property
    def route(self):
        return f"{self.source.name} -> {self.destination.name}"

    def __str__(self):
        return self.route


class Flight(models.Model):
    route = models.ForeignKey(
        to=Route,
        related_name="flights",
        on_delete=models.SET_NULL,
        null=True
    )
    airplane = models.ForeignKey(
        to=Airplane,
        related_name="flights",
        on_delete=models.SET_NULL,
        null=True
    )
    departure_time = models.DateTimeField()
    arrival_time = models.DateTimeField()

    class Meta:
        app_label = "airport"

    @property
    def flight_time(self):
        flight_duration = self.arrival_time - self.departure_time
        hours = flight_duration.seconds // 3600
        minutes = (flight_duration.seconds // 60) % 60
        return f"{hours} hours {minutes} minutes"

    def __str__(self):
        return f"{self.route}({self.departure_time} - {self.arrival_time})"


class Crew(models.Model):
    first_name = models.CharField(max_length=63)
    last_name = models.CharField(max_length=63)
    flight = models.ManyToManyField(
        to=Flight,
    )

    class Meta:
        app_label = "airport"

    @property
    def full_name(self):
        return f"{self.first_name} {self.last_name}"

    def __str__(self):
        return self.full_name


class Order(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        related_name="orders",
        on_delete=models.PROTECT
    )

    class Meta:
        app_label = "airport"
        ordering = ["-created_at"]

    def __str__(self):
        return self.created_at


class Ticket(models.Model):
    row = models.IntegerField()
    seat = models.IntegerField()
    flight = models.ForeignKey(
        to=Flight,
        related_name="tickets",
        on_delete=models.PROTECT,
    )
    order = models.ForeignKey(
        to=Order,
        related_name="tickets",
        on_delete=models.CASCADE
    )

    class Meta:
        unique_together = ["seat", "row", "flight"]
        app_label = "airport"

    @staticmethod
    def validate_ticket(row, seat, airplane, error_to_raise):
        for ticket_attr_value, ticket_attr_name, airplane_attr_name in [
            (row, "row", "rows"),
            (seat, "seat", "seats_in_row"),
        ]:
            count_attrs = getattr(airplane, airplane_attr_name)
            if not (1 <= ticket_attr_value <= count_attrs):
                raise error_to_raise(
                    {
                        ticket_attr_name: f"{ticket_attr_name} "
                                          f"number must be in available range: "
                                          f"(1, {airplane_attr_name}): "
                                          f"(1, {count_attrs})"
                    }
                )

    def clean(self):
        Ticket.validate_ticket(
            self.row,
            self.seat,
            self.flight.airplane,
            ValidationError,
        )

    def save(
            self,
            force_insert=False,
            force_update=False,
            using=None,
            update_fields=None,
    ):
        self.full_clean()
        return super(Ticket, self).save(
            force_insert, force_update, using, update_fields
        )
